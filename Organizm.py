from Polozenie import Polozenie
from RandomMachine import RandomMachine


class Organizm:
    age = 0
    sila = None
    inicjatywa = None
    polozenie = None
    symbol = None
    world = None

    def __init__(self, world, polozenie):
        self.world = world
        self.polozenie = polozenie

    def isInsizeBox(self, polozenie):
        return 0 <= polozenie.getX() < self.world.getWidth() and 0 <= polozenie.getY() < self.world.getHeight()

    def getNerbayPositions(self, maximum=1):
        positions = []
        for relativePosition in self.world.getPossibleDirections():
            pos = self.getPolozenie().add(relativePosition)
            if not pos.equals(self.getPolozenie()) and self.isInsizeBox(pos):
                add = True
                for el in positions:
                    if el.equals(pos):
                        add = False
                if add:
                    positions.append(pos)
            if maximum == 2:
                for relativePosition2 in self.world.getPossibleDirections():
                    pos2 = pos.add(relativePosition2)
                    if not pos2.equals(self.getPolozenie()) and self.isInsizeBox(pos2):
                        add = True
                        for el in positions:
                            if el.equals(pos2):
                                add = False
                        if add:
                            positions.append(pos2)
        return positions

    def getNerbayPosition(self, maximum=1):
        positions = self.getNerbayPositions(maximum)
        if len(positions) > 0:
            id = RandomMachine().getNextInt(len(positions))
            return positions[id]
        else:
            return None

    def isTheSameTypeAs(self, organizm):
        return type(self) is type(organizm)

    def getSila(self):
        return self.sila

    def getInicjatywa(self):
        return self.inicjatywa

    def getPolozenie(self):
        return self.polozenie

    def setPolozenie(self, polozenie):
        self.polozenie = polozenie

    def getAge(self):
        return self.age

    def addAge(self):
        self.age += 1

    def getSymbol(self):
        return self.symbol

    def addSila(self, x):
        self.sila += x
        self.world.echo("Dodawanie sily")

    def czyOdbilAtak(self, organizm):
        return organizm.getSila() < self.getSila()

    def writeToFile(self, file):
        file.write(self.symbol)
        file.write('\n')
        file.write(str(self.sila))
        file.write('\n')
        file.write(str(self.inicjatywa))
        file.write('\n')
        file.write(str(self.age))
        file.write('\n')
        file.write(str(self.polozenie.getX()))
        file.write('\n')
        file.write(str(self.polozenie.getY()))
        file.write('\n')

    def readFromFile(self, file):
        self.sila = int(next(file))
        self.inicjatywa = int(next(file))
        self.age = int(next(file))
        x = int(next(file))
        y = int(next(file))
        self.polozenie = Polozenie(x, y)

    def compare(self):
        return -(1000 * self.getInicjatywa() + self.getSila())

    def createChild(self, polozenie):
        from FabrykaOrganizmow import FabrykaOrganizmow
        return FabrykaOrganizmow().build(self.symbol, self.world, polozenie)



