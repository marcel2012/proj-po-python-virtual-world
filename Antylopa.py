from RandomMachine import RandomMachine
from Zwierze import Zwierze


class Antylopa (Zwierze):
    def __init__(self, world, polozenie):
        super().__init__(world, polozenie)
        self.sila = 4
        self.inicjatywa = 4
        self.symbol = 'A'

    def czyOdbilAtak(self, organizm):
        if RandomMachine().getNextInt() < 50:
            pos = self.getFreeNerbayPosition()
            if pos is not None:
                self.world.echo("Antylopa ucieka przed walka")
            return True
        else:
            return super().czyOdbilAtak(organizm)

    def getNerbayPosition(self, maximum=2):
        return super().getNerbayPosition(maximum)

