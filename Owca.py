from Zwierze import Zwierze


class Owca (Zwierze):
    def __init__(self, world, polozenie):
        super().__init__(world, polozenie)
        self.sila = 4
        self.inicjatywa = 4
        self.symbol = 'O'
