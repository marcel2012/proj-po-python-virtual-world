from RandomMachine import RandomMachine
from Zwierze import Zwierze


class Lis (Zwierze):
    def __init__(self, world, polozenie):
        super().__init__(world, polozenie)
        self.sila = 3
        self.inicjatywa = 7
        self.symbol = 'L'

    def getNerbayPosition(self, maximum=1):
        positions = self.getNerbayPositions(maximum)
        freePositions = []
        for pos in positions:
            if self.world.isPolozenieEmpty(pos) or self.world.getOrganizmByPosition(pos).getSila() < self.getSila():
                freePositions.append(pos)
        if len(freePositions) > 0:
            id = RandomMachine().getNextInt(len(freePositions))
            return freePositions[id]
        else:
            return None
