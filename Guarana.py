from Roslina import Roslina


class Guarana (Roslina):
    def __init__(self, world, polozenie):
        super().__init__(world, polozenie)
        self.sila = 0
        self.symbol = 'g'

    def czyOdbilAtak(self, organizm):
        if organizm.getSila() < self.getSila():
            return True
        else:
            organizm.addSila(3)
            return False
