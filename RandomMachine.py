import random

from Polozenie import Polozenie


class RandomMachine:
    def getNextInt(self, maximum=100):
        return random.randint(0, maximum-1)

    def getRandomPosition(self, maxX, maxY):
        return Polozenie(self.getNextInt(maxX), self.getNextInt(maxY))
