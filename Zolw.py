from RandomMachine import RandomMachine
from Zwierze import Zwierze


class Zolw (Zwierze):
    def __init__(self, world, polozenie):
        super().__init__(world, polozenie)
        self.sila = 2
        self.inicjatywa = 1
        self.symbol = 'Z'

    def akcja(self):
        if RandomMachine().getNextInt() < 75:
            self.world.echo("Zółw nie robi nic")
        else:
            super().akcja()

    def czyOdbilAtak(self, organizm):
        return organizm.getSila() < 5
