from Owca import Owca
from Polozenie import Polozenie


class CyberOwca(Owca):
    def __init__(self, world, polozenie):
        super().__init__(world, polozenie)
        self.sila = 11
        self.symbol = '8'

    def getNerbayPositions(self, maximum=1):
        positionsOfBarszcz = []
        matrix = []
        comeFrom = []
        for y in range(self.world.getHeight()):
            matrix.append([])
            comeFrom.append([])
            for x in range(self.world.getWidth()):
                matrix[y].append(-1)
                comeFrom[y].append(None)
                pos = Polozenie(x, y)
                org = self.world.getOrganizmByPosition(pos)
                if org is not None and org.getSymbol() == 'b':
                    positionsOfBarszcz.append(pos)
                    matrix[y][x] = -2
        if len(positionsOfBarszcz) > 0:
            toVisit = [self.getPolozenie()]
            while len(toVisit) > 0:
                pos = toVisit[0]
                toVisit.remove(pos)
                if matrix[pos.getY()][pos.getX()] == -2:
                    now = pos
                    prev = now
                    self.world.echo("Najbliższy barszcz jest na polu (" + str(now.getX())
                                    + ", " + str(now.getY()) + ")")
                    while not now.equals(self.getPolozenie()):
                        prev = now
                        now = comeFrom[now.getY()][now.getX()]
                    self.world.echo("aby się tam dostać idę na (" + str(prev.getX())
                                    + ", " + str(prev.getY()) + ")")
                    return [prev]
                elif matrix[pos.getY()][pos.getX()] == -1:
                    matrix[pos.getY()][pos.getX()] = 0
                    for relativePosition in self.world.getPossibleDirections():
                        newPos = pos.add(relativePosition)
                        if self.isInsizeBox(newPos) and matrix[newPos.getY()][newPos.getX()] < 0:
                            toVisit.append(newPos)
                            if comeFrom[newPos.getY()][newPos.getX()] is None:
                                comeFrom[newPos.getY()][newPos.getX()] = pos
        else:
            return super().getNerbayPositions(maximum)
