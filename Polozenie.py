class Polozenie:
    x = None
    y = None

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def equals(self, polozenie):
        return self.x == polozenie.x and self.y == polozenie.y

    def add(self, polozenie):
        return Polozenie(self.x + polozenie.x, self.y + polozenie.y)