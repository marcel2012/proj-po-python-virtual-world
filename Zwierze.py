from Organizm import Organizm
from RandomMachine import RandomMachine


class Zwierze (Organizm):
    world = None

    def __init__(self, world, polozenie):
        super().__init__(world, polozenie)

    def getFreeNerbayPosition(self):
        positions = self.getNerbayPositions(1)
        freePositions = []
        for pos in positions:
            if self.world.isPolozenieEmpty(pos):
                freePositions.append(pos)
        if len(freePositions) > 0:
            id = RandomMachine().getNextInt(len(freePositions))
            return freePositions[id]
        else:
            return None

    def akcja(self):
        newPosition = self.getNerbayPosition()
        if newPosition is not None:
            if self.world.isPolozenieEmpty(newPosition):
                self.setPolozenie(newPosition)
            else:
                self.kolizja(newPosition)

    def kolizja(self, newPosition):
        org = self.world.getOrganizmByPosition(newPosition)
        if self.isTheSameTypeAs(org):
            freePosition = self.getFreeNerbayPosition()
            if freePosition is not None:
                newOrg = self.createChild(freePosition)
                self.world.echo("powstaje nowe zwierze " + self.symbol)
                self.world.addOrganizm(newOrg)
        elif org.czyOdbilAtak(self):
            self.world.echo(org.getSymbol() + " odbił atak " + self.symbol)
        else:
            self.world.echo(org.getSymbol() + " nie odbil ataku " + self.symbol)
            self.world.deleteOrganizm(org)
            self.setPolozenie(newPosition)

