import tkinter
import tkinter.messagebox
from tkinter.simpledialog import askinteger
import math

from FabrykaOrganizmow import FabrykaOrganizmow
from Organizm import Organizm
from Polozenie import Polozenie
from RandomMachine import RandomMachine
from Antylopa import Antylopa
from Barszcz import Barszcz
from Czlowiek import Czlowiek
from Guarana import Guarana
from Jagody import Jagody
from Lis import Lis
from Mlecz import Mlecz
from Owca import Owca
from Trawa import Trawa
from Wilk import Wilk
from Zolw import Zolw
from CyberOwca import CyberOwca


class Swiat:
    saveFileName = "save.txt"
    MAX_TRIES = 100
    width = None
    height = None
    organizmy = []
    toRemove = []
    toAdd = []
    isHex = False
    possibleDirectionsForCartesian = [Polozenie(1, 0), Polozenie(-1, 0), Polozenie(0, 1), Polozenie(0, -1)]
    possibleDirectionsForHex = possibleDirectionsForCartesian + [Polozenie(1, -1), Polozenie(-1, 1)]
    czlowiek = None
    frame = None
    pressedKey = None
    organizmyButtons = []
    selectList = None
    selectedPosition = None
    text = None
    maxSize = 28

    def __init__(self):
        self.flushToAddBuffer()
        self.flushToRemoveBuffer()

        self.frame = tkinter.Tk()
        self.frame.title('Wirtualny świat - Marcel Korpal 175911')

        startButton = tkinter.Button(self.frame, text="Start", command=self.init, width=20)
        startButton.place(x=0, y=0)

        self.frame.geometry("600x700")
        self.frame.mainloop()

    def addOrganizmListClick(self, event):
        w = event.widget
        index = int(w.curselection()[0])
        c = w.get(index)[0]
        self.addOrganizm(FabrykaOrganizmow().build(c, self, self.selectedPosition), True)
        self.hideSelectList()

    def showSelectList(self, position):
        self.hideSelectList()
        self.selectedPosition = position
        self.selectList = tkinter.Listbox(self.frame)
        self.selectList.insert(1, "Antylopa")
        self.selectList.insert(2, "barszcz")
        self.selectList.insert(3, "guarana")
        self.selectList.insert(4, "jagody")
        self.selectList.insert(5, "Lis")
        self.selectList.insert(6, "mlecz")
        self.selectList.insert(7, "Owca")
        self.selectList.insert(8, "trawa")
        self.selectList.insert(9, "Wilk")
        self.selectList.insert(10, "Zolw")
        self.selectList.insert(11, "8 CyberOwca")
        self.selectList.place(x=200, y=self.height * (self.maxSize + 3))
        self.selectList.bind('<<ListboxSelect>>', self.addOrganizmListClick)

    def hideSelectList(self):
        if self.selectList is not None:
            self.selectList.destroy()
        self.selectList = None

    def pressKey(self, event):
        self.pressedKey = event.keycode
        self.start(True)

    def changeToHex(self):
        self.isHex = True
        self.windowResize()
        self.rysujSwiat()

    def changeFromHex(self):
        self.isHex = False
        self.windowResize()
        self.rysujSwiat()

    def init(self):
        while self.width is None or self.width < 5:
            self.width = askinteger('Szerokość', 'Podaj szerokość')
        while self.height is None or self.height < 5:
            self.height = askinteger('Wysokość', 'Podaj wysokość')

        self.createOrganizmy()

        nextRoundButton = tkinter.Button(self.frame, text="Następna runda", command=self.start, width=20)
        nextRoundButton.place(x=0, y=0)

        saveToFileButton = tkinter.Button(self.frame, text="Zapisz do pliku", command=self.writeToFile, width=20)
        saveToFileButton.place(x=0, y=60)

        readFromFileButton = tkinter.Button(self.frame, text="Odczyt z pliku", command=self.readFromFile, width=20)
        readFromFileButton.place(x=0, y=90)

        extraPowerButton = tkinter.Button(self.frame, text="Super moc", command=self.addSuperpower, width=20)
        extraPowerButton.place(x=0, y=150)

        exitButton = tkinter.Button(self.frame, text="Wyjście", command=self.exit, width=20)
        exitButton.place(x=0, y=240)

        changeToHex = tkinter.Button(self.frame, text="Hex", command=self.changeToHex, width=7)
        changeToHex.place(x=0, y=180)

        changeFromHex = tkinter.Button(self.frame, text="Kart", command=self.changeFromHex, width=7)
        changeFromHex.place(x=100, y=180)

        self.text = tkinter.Text(self.frame, width=26)
        self.text.place(x=0, y=270)

        self.rysujSwiat()
        self.frame.bind('<Left>', self.pressKey)
        self.frame.bind('<Right>', self.pressKey)
        self.frame.bind('<Down>', self.pressKey)
        self.frame.bind('<Up>', self.pressKey)
        self.frame.bind('f', self.pressKey)
        self.frame.bind('h', self.pressKey)
        self.frame.bind('t', self.pressKey)
        self.frame.bind('y', self.pressKey)
        self.frame.bind('v', self.pressKey)
        self.frame.bind('b', self.pressKey)

    def addSuperpower(self):
        if self.czlowiek is not None:
            self.czlowiek.wlaczMoc()

    def getPressedKey(self):
        return self.pressedKey

    def exit(self):
        self.frame.destroy()

    def getPossibleDirections(self):
        if self.isHex:
            return self.possibleDirectionsForHex
        else:
            return self.possibleDirectionsForCartesian

    def isOrganizmOnBoard(self, organizm):
        return organizm in self.organizmy

    def createOrganizmy(self):
        self.createNewBoard()

        self.organizmy.append(Antylopa(self, self.getFreePosition()))
        self.organizmy.append(Antylopa(self, self.getFreePosition()))

        self.organizmy.append(Barszcz(self, self.getFreePosition()))
        self.organizmy.append(Barszcz(self, self.getFreePosition()))

        self.organizmy.append(Guarana(self, self.getFreePosition()))
        self.organizmy.append(Guarana(self, self.getFreePosition()))

        self.organizmy.append(Jagody(self, self.getFreePosition()))
        self.organizmy.append(Jagody(self, self.getFreePosition()))

        self.organizmy.append(Lis(self, self.getFreePosition()))
        self.organizmy.append(Lis(self, self.getFreePosition()))

        self.organizmy.append(Mlecz(self, self.getFreePosition()))
        self.organizmy.append(Mlecz(self, self.getFreePosition()))

        self.organizmy.append(Owca(self, self.getFreePosition()))
        self.organizmy.append(Owca(self, self.getFreePosition()))

        self.organizmy.append(Trawa(self, self.getFreePosition()))
        self.organizmy.append(Trawa(self, self.getFreePosition()))

        self.organizmy.append(Wilk(self, self.getFreePosition()))
        self.organizmy.append(Wilk(self, self.getFreePosition()))

        self.organizmy.append(Zolw(self, self.getFreePosition()))
        self.organizmy.append(Zolw(self, self.getFreePosition()))

        self.organizmy.append(Owca(self, self.getFreePosition()))
        self.organizmy.append(Owca(self, self.getFreePosition()))

        self.organizmy.append(CyberOwca(self, self.getFreePosition()))
        self.organizmy.append(CyberOwca(self, self.getFreePosition()))

        self.czlowiek = Czlowiek(self, self.getFreePosition())
        self.organizmy.append(self.czlowiek)

    def createNewBoard(self):
        self.organizmy = []
        self.czlowiek = None
        self.windowResize()

    def windowResize(self):
        additionX = 0
        if self.isHex:
            additionX = self.maxSize / 2 * self.height
        x = 220 + (self.maxSize + 3) * self.width + additionX
        y = (self.maxSize + 3) * self.height
        y = max(650, y)
        self.frame.geometry(str(int(x)) + "x" + str(int(y)))

    def sortOrganizmy(self):
        self.organizmy.sort(key=Organizm.compare)

    def getFreePosition(self):
        for i in range(0, self.MAX_TRIES):
            position = RandomMachine().getRandomPosition(self.width, self.height)
            if self.isPolozenieEmpty(position):
                return position
        return None

    def addOrganizm(self, organizm, instant=False):
        self.toAdd.append(organizm)
        if instant:
            self.flushToAddBuffer()
        self.rysujSwiat()

    def wykonajTure(self):
        self.sortOrganizmy()
        for org in self.organizmy:
            if org not in self.toRemove:
                org.akcja()
                org.addAge()
        self.flushToAddBuffer()
        self.flushToRemoveBuffer()

    def getOrganizmByPosition(self, position):
        for org in self.organizmy:
            if org.getPolozenie().equals(position):
                return org
        return None

    def flushToAddBuffer(self):
        self.organizmy += self.toAdd
        self.toAdd = []

    def flushToRemoveBuffer(self):
        if self.czlowiek in self.toRemove:
            self.czlowiek = None
        for org in self.toRemove:
            if org in self.organizmy:
                self.organizmy.remove(org)
        self.toRemove = []

    def start(self, asHuman=False):
        if self.czlowiek is None or asHuman:
            self.flushDebugWindow()
            self.wykonajTure()
            self.rysujSwiat()

    def flushDebugWindow(self):
        self.text.delete('1.0', tkinter.END)

    def rysujSwiat(self):
        for button in self.organizmyButtons:
            button.place_forget()
            button.delete("all")
            button.destroy()
        self.organizmyButtons = []
        for x in range(self.width):
            for y in range(self.height):
                pos = Polozenie(x, y)
                org = self.getOrganizmByPosition(pos)
                button = tkinter.Canvas(self.frame, width=self.maxSize + 2, height=self.maxSize + 2)
                if self.isHex:
                    points = []
                    alpha = math.pi / 6.0
                    for i in range(6):
                        points.append(int(math.sin(alpha) * self.maxSize / 2 + self.maxSize / 2))
                        points.append(int(math.cos(alpha) * self.maxSize / 2 + self.maxSize / 2))
                        alpha += math.pi / 3.0
                    button.create_polygon(points, fill='#fff')
                else:
                    button.create_rectangle(2, 2, self.maxSize - 2, self.maxSize - 2, fill="#fff", outline="#fff")
                if org is not None:
                    button.create_text(10, 15, anchor=tkinter.W, text=org.getSymbol())
                else:
                    button.bind("<Button-1>", lambda event, pos=pos: self.showSelectList(pos))
                additionX = 0
                if self.isHex:
                    additionX = self.maxSize / 2 * y
                button.place(x=int(200 + additionX + x * (self.maxSize + 3)), y=y * (self.maxSize + 3))
                self.organizmyButtons.append(button)

    def isPolozenieEmpty(self, polozenie):
        return self.getOrganizmByPosition(polozenie) is None

    def getWidth(self):
        return self.width

    def getHeight(self):
        return self.height

    def deleteOrganizm(self, organizm):
        self.toRemove.append(organizm)

    def writeToFile(self):
        file = open(self.saveFileName, 'w')
        if self.isHex:
            file.write(str(1))
        else:
            file.write(str(0))
        file.write('\n')
        file.write(str(self.width))
        file.write('\n')
        file.write(str(self.height))
        file.write('\n')
        file.write(str(len(self.organizmy)))
        file.write('\n')
        for org in self.organizmy:
            org.writeToFile(file)
        file.close()

    def readFromFile(self):
        file = open(self.saveFileName, 'r')
        isHex = int(next(file))
        self.isHex = isHex == 1
        self.width = int(next(file))
        self.height = int(next(file))
        length = int(next(file))
        self.createNewBoard()
        for i in range(length):
            c = next(file)[0]
            position = Polozenie(0, 0)
            org = FabrykaOrganizmow().build(c, self, position)
            if org is not None:
                if org.getSymbol() == '@':
                    self.czlowiek = org
                org.readFromFile(file)
                self.organizmy.append(org)
            else:
                self.echo("### Nieznane zwierze w pliku")
        self.rysujSwiat()

    def echo(self, string):
        self.text.insert(tkinter.INSERT, string + '\n')


