from Organizm import Organizm
from RandomMachine import RandomMachine


class Roslina (Organizm):
    def __init__(self, world, polozenie):
        super().__init__(world, polozenie)
        self.inicjatywa = 0

    def akcja(self):
        if RandomMachine().getNextInt() < 40:
            newPosition = self.getNerbayPosition()
            if newPosition is not None and self.world.isPolozenieEmpty(newPosition):
                newOrg = self.createChild(newPosition)
                self.world.addOrganizm(newOrg)
