from Zwierze import Zwierze


class Wilk (Zwierze):
    def __init__(self, world, polozenie):
        super().__init__(world, polozenie)
        self.sila = 9
        self.inicjatywa = 5
        self.symbol = 'W'
