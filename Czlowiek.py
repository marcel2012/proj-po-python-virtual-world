from KeyBinding import *
from Polozenie import Polozenie
from RandomMachine import RandomMachine
from Zwierze import Zwierze


class Czlowiek (Zwierze):
    dodatkowaMoc = 0

    def __init__(self, world, polozenie):
        super().__init__(world, polozenie)
        self.sila = 5
        self.inicjatywa = 4
        self.symbol = '@'

    def czyOdbilAtak(self, organizm):
        if RandomMachine().getNextInt() < 50:
            pos = self.getFreeNerbayPosition()
            if pos is not None:
                self.world.echo("Antylopa ucieka przed walka")
            return True
        else:
            return super().czyOdbilAtak(organizm)

    def wlaczMoc(self):
        if self.dodatkowaMoc == 0:
            self.world.echo("--> Człowiek dostaje super moc")
            self.dodatkowaMoc = 10
        else:
            self.world.echo("--> Człowiek ma już aktywną moc")

    def addAge(self):
        self.age += 1
        if self.dodatkowaMoc > 0:
            self.dodatkowaMoc -= 1
            self.world.echo("--> Człowiek ma teraz moc " + str(self.dodatkowaMoc))

    def getSila(self):
        return self.sila + self.dodatkowaMoc

    def writeToFile(self, file):
        super().writeToFile(file)
        file.write(str(self.dodatkowaMoc))
        file.write('\n')

    def readFromFile(self, file):
        super().readFromFile(file)
        self.dodatkowaMoc = int(next(file))

    def getNerbayPositions(self, maximum=1):
        positions = []
        x = 0
        y = 0
        key = self.world.getPressedKey()
        if key == ARROW_UP or key == KEY_TOP_LEFT:
            y -= 1
        elif key == ARROW_DOWN or key == KEY_BOTTOM_RIGHT:
            y += 1
        elif key == ARROW_RIGHT or key == KEY_RIGHT:
            x += 1
        elif key == ARROW_LEFT or key == KEY_LEFT:
            x -= 1
        elif key == KEY_TOP_RIGHT:
            x += 1
            y -= 1
        elif key == KEY_BOTTOM_LEFT:
            x -= 1
            y += 1
        relativePosition = Polozenie(x, y)
        for pos in self.world.getPossibleDirections():
            if pos.equals(relativePosition):
                pos = self.getPolozenie().add(relativePosition)
                if self.isInsizeBox(pos):
                    positions.append(pos)
        return positions
        

