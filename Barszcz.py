from Roslina import Roslina
from Zwierze import Zwierze


class Barszcz (Roslina):
    def __init__(self, world, polozenie):
        super().__init__(world, polozenie)
        self.sila = 10
        self.symbol = 'b'

    def isAnimal(self, org):
        return isinstance(org, Zwierze)

    def akcja(self):
        self.world.echo("barszcz zabija wszystko dookoła")
        positions = self.getNerbayPositions()
        for pos in positions:
            org = self.world.getOrganizmByPosition(pos)
            if self.isAnimal(org) and org.getSila() < self.getSila():
                self.world.deleteOrganizm(org)
