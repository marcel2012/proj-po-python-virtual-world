class FabrykaOrganizmow:
    def build(self, symbol, world, position):
        from Antylopa import Antylopa
        from Barszcz import Barszcz
        from Czlowiek import Czlowiek
        from Guarana import Guarana
        from Jagody import Jagody
        from Lis import Lis
        from Mlecz import Mlecz
        from Owca import Owca
        from Trawa import Trawa
        from Wilk import Wilk
        from Zolw import Zolw
        from CyberOwca import CyberOwca
        return {
            'A': Antylopa(world, position),
            'b': Barszcz(world, position),
            '@': Czlowiek(world, position),
            'g': Guarana(world, position),
            'j': Jagody(world, position),
            'L': Lis(world, position),
            'm': Mlecz(world, position),
            'O': Owca(world, position),
            't': Trawa(world, position),
            'W': Wilk(world, position),
            'Z': Zolw(world, position),
            '8': CyberOwca(world, position)
        }[symbol]
